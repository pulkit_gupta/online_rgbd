## Synopsis
This is a real-time RGBD server that uses socket.io and zeromq

Online RGB-D streaming ( on Web browser from arm device using Zeromq midd)
In this application, a web page is developed with 3 buttons to switch between RGB, Depth and IR video stream over the internet on web browser,
Arm remote device with Kinect sensor communicates with nodeJS server through ZeroMQ and Nodejs on realtime transfer the RGBD data to web browser after authentication.

User can change between different remote devices by the unique ID of the ARM devices.

## Prerequisites 
You will need to have node.js and npm installed.

## Installing the demo
Once you have node.js and npm installed you can install the application's dependencies.

```bash

  npm install

```

## Running the demo

```bash

  node server/server.js

```

```bash

  sudo node server/server.js

```