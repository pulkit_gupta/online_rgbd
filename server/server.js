
  var fs = require('fs'),
    path = require('path'),
    sio = require('socket.io'),
    static = require('node-static');
    //var cv = require('/usr/lib/node_modules/opencv');
    var zmq = require('zmq');
    var subscriber = zmq.socket('sub');
    var publisher = zmq.socket('pub');
    
    var filter = "";
    subscriber.subscribe(filter);

  var app = require('http').createServer(handler);
  var i =0;

  
  app.listen(8000, function()
    {
      console.log("listening server on ip: 134.109.5.21 port 8000.");         
    });


  var file = new static.Server(path.join(__dirname, '..', 'public'));
  function handler(req, res) {
    file.serve(req, res);
  }

 // var sockets_live = new Array({"name" : "p1", "socket": null}, {"name" : "p2", "socket": null} );
  var sockets_live = new Array();

  var io = sio.listen(app);

  io.sockets.on('connection', function (socket) {
        
    socket.emit('user message', " server says: hello, You are connected");      
    var image = 0;
    //socket.emit('user image', image);  

    socket.on('register', function(name){
        if(sockets_live[socket.name]!= undefined)
        {
          sockets_live[socket.name]= undefined;  //deleting old registeration, because no two names can have socket at the same time
        }
        socket.name = name;
        sockets_live[name] = socket;
        //console.log(name+ " is added/registered in list as object = "+ sockets_live[name].id);
        sockets_live[name].emit('user message', "Now you are registered and confirmed as "+name);
    });

    subscriber.on('message', function(data) {
      //console.log(data.toString());
      var string_data = data.toString();
      var temp = string_data.split(".");

      var socket_index = temp[0], image_string = temp[1];

      console.log("sending to socket with id = "+socket_index);
      var socket_var = sockets_live[socket_index];
      console.log("object is "+socket_var);
      if(socket_var != undefined)
        {
          socket_var.emit('image_rgb', image_string);
        }
      else
      {
        console.log("not send image frames");        
      }
    });

    socket.on('disconnect', function (){
      console.log("CLIENT disconnect");
      console.log("spliced the socket object");

    });  
    
    socket.on('command', function(val){
      console.log("client asked for camera");
      var strvalue = socket.name+"."+val+".";
      publisher.send(strvalue); 
      console.log(strvalue);
      
    });
  });
   subscriber.bind("tcp://199.175.49.215 :8001");
   publisher.bind("tcp://199.175.49.215 :8002");
  //  subscriber.bind("tcp://134.109.5.21:8001");
    //publisher.bind("tcp://134.109.5.21:8002");
  
  /*
  31-july-1988
  12:15 pm
  rohtak  
  */