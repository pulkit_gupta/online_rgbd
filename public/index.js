
  //
  // socket.io code
  //
  var socket = io.connect("http://pulkit.eu:8000");
  var clientid = 0;
  socket.on('connect', function () {
    $('#main').append($('<p>').append("You are connected."));
  });
  socket.on('user message', message);
  socket.on('image_rgb', image_rgb);
  socket.on('image_canny', image_canny);
 
  function message (msg) {
    $('#main').append($('<p>').append(msg));
  }


  function image_rgb (base64Image) {  
    var ImageElm_rgb =  document.getElementById ("imgelem_rgb");
    ImageElm_rgb.src =  'data: image / jpeg; base64,'  +  base64Image;
  }

  
  function image_canny (base64Image) {
    var ImageElm_canny =  document.getElementById ("imgelem_canny");  
    ImageElm_canny.src =  'data: image / jpeg; base64,'  +  base64Image;
  }

  //
  // dom manipulation code
  //
 
